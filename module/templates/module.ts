module <%= namespace %> {
	var main = angular.module('<%= _.camelize(name) %>', ['ui.bootstrap','ui.utils','<%= routerModuleName %>','ngAnimate'
		/* Inject Modules Above */
		]);
	main.config(routeConfig);

	<% if(!uirouter) { %>
	routeConfig.$inject = ["$routeProvider"];
	function routeConfig($routeProvider: ng.route.IRouteProvider): void {

	    /* Add New Routes Above */

	}
<% } %><% if(uirouter) { %>
	routeConfig.$inject = ["$stateProvider"];
	function routeConfig($stateProvider: ng.ui.IStateProvider): void {

	    /* Add New Routes Above */

	}
<% } %>}