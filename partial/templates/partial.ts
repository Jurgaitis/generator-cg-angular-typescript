module <%= namespace %>{

	export class <%= ctrlname %>{
		public static IID: string = '<%= ctrlname %>';
		public static $inject: string[] = [];

		constructor(){
			
		}
	}
	
	angular.module('<%= appname %>').controller(<%= ctrlname %>.IID, <%= ctrlname %>);
}