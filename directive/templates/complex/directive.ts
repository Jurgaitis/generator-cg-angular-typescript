module <%= namespace %>
{
	export class <%= _.camelize(name) %> implements ng.IDirective {
		restrict = 'A';
		replace = true;
		scope = {

		}
		templateUrl= '<%= htmlPath %>';

		constructor(){

		}

		link = (scope: ng.IScope, element: ng:IAugmentedJquery, attrs: ng.IAttributes, ctrl: any) => {

		}

		static factory(): ng.IDirectiveFactory {
			const directive = () => new <%= _.camelize(name) %>();
			directive.$inject = [];
			return directive;
		}

		static IID: string = '<%= _.camelize(name) %>';
	}

	angular.module('<%= appname %>').directive(<%= _.camelize(name) %>.IID, <%= _.camelize(name) %>.factory());
}