module <%= namespace %>{

	export interface <%= 'I' + _.camelize(name) %>{

	}

	export class <%= _.camelize(name) %> implements <%= 'I' + _.camelize(name) %>{
		public static IID: string = '<%= _.camelize(name) %>';
		public static $inject: string[] = [];

		constructor(){

		}<% if (!service) { %>

		static getInstance(){
			const factory = () => new <%= _.camelize(name) %>();
			factory.$inject = [];
			return factory;
		}
	<% } %>}
	<% if (!service) { %>
	angular.module('<%= appname %>').factory(<%= _.camelize(name) %>.IID, <%= _.camelize(name) %>.getInstance());
	<% } %><% if (service) { %>
	angular.module('<%= appname %>').service(<%= _.camelize(name) %>.IID, <%= _.camelize(name) %>);
<% } %>}